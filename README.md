# 74181\_ALU

   This project aims to implement the first ALU ever created in a simple, concise and efficient way.

# Installation
   To install this program, go to the ./build directory and run `make`.

   You can use a symbolic link such as `ln -s /path/to/bin/74181_ALU /usr/bin` if you want to use the program in the CLI without explicitly showing the path of the binary.

# Usage
   To use this program, you have to specify 5 arguments:

   1. 4 bits for the S inputs
   2. 4 bits for the A inputs
   3. 4 bits for the B inputs
   4. 1 bit for the M input
   5. 1 bit for the C input

   An example of usage would be:

   ```
   ./74181 1111 0101 1010 1 0
   ```

   The S bits select for the operation to be performed, and the A and B are the input numbers.
   M is the bit that selected the "mode" of the operation, and M == 1 sets "logic" mode (for logical operations like "A XOR B") and M == 0 sets "arithmetic" mode (for arithmetic/"mathematic" operations like additions, substractions, etc...).

# Design

   You can look at the design of the ALU with this URL:

   [Here is the sheet that specifies the operations](https://cloud.brassart.xyz/s/y2Fq9GTipb7niZk)

   [Here is the layout of the logic gates used to implement the code](https://cloud.brassart.xyz/s/LwDsCKic9G38xB4)

   Or if you want more general information, [here is the wikipedia page](https://en.wikipedia.org/wiki/74181)


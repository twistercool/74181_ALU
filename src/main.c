#include "header.h"

int main(int argc, char* argv[]) {
   if (argc != 6 || strlen(argv[1]) != 4 || strlen(argv[2]) != 4 ||
       strlen(argv[3]) != 4 || strlen(argv[4]) != 1 || strlen(argv[5]) != 1) {
      fprintf(stderr, "Usage: ./apu <4 bits for S inputs> <4 bits for A inputs> "
                      "<4 bits for B inputs> <1 bit for M> <1 bit for C>");

      return 1;
   }

   GATE* inputs_S[4];

   inputs_S[0] = new_gate(out_gate, argv[1][0] - '0');
   inputs_S[1] = new_gate(out_gate, argv[1][1] - '0');
   inputs_S[2] = new_gate(out_gate, argv[1][2] - '0');
   inputs_S[3] = new_gate(out_gate, argv[1][3] - '0');

   GATE* inputs_A[4];

   inputs_A[0] = new_gate(out_gate, argv[2][0] - '0');
   inputs_A[1] = new_gate(out_gate, argv[2][1] - '0');
   inputs_A[2] = new_gate(out_gate, argv[2][2] - '0');
   inputs_A[3] = new_gate(out_gate, argv[2][3] - '0');

   GATE* inputs_B[4];

   inputs_B[0] = new_gate(out_gate, argv[3][1] - '0');
   inputs_B[1] = new_gate(out_gate, argv[3][2] - '0');
   inputs_B[2] = new_gate(out_gate, argv[3][3] - '0');
   inputs_B[3] = new_gate(out_gate, argv[3][4] - '0');

   GATE* input_M = new_gate(out_gate, argv[4] - '0');
   GATE* input_C = new_gate(out_gate, argv[5] - '0');

   // first row of NOT gates: starts from the "bottom" aka next to the C and M
   // inputs on the ALU blueprint
   GATE* not_0[5];

   not_0[0] = new_gate(not, input_M);
   not_0[1] = new_gate(not, inputs_B[0]);
   not_0[2] = new_gate(not, inputs_B[1]);
   not_0[3] = new_gate(not, inputs_B[2]);
   not_0[4] = new_gate(not, inputs_B[3]);

   // second row: and gates
   GATE* and_1[16];

   and_1[0] = new_gate(and, inputs_A[0], inputs_S[0]);
   and_1[1] = new_gate(and, inputs_S[1], not_0[1]);
   and_1[2] = new_gate(triple_and, not_0[1], inputs_S[2], inputs_A[0]);
   and_1[3] = new_gate(triple_and, inputs_A[0], inputs_S[3], inputs_B[0]);
   and_1[4] = new_gate(and, inputs_B[1], inputs_S[0]);
   and_1[5] = new_gate(and, inputs_S[1], not_0[2]);
   and_1[6] = new_gate(triple_and, not_0[2], inputs_S[2], inputs_A[1]);
   and_1[7] = new_gate(triple_and, inputs_A[1], inputs_S[3], inputs_B[1]);
   and_1[8] = new_gate(and, inputs_B[2], inputs_S[0]);
   and_1[9] = new_gate(and, inputs_S[1], not_0[3]);
   and_1[10] = new_gate(triple_and, not_0[3], inputs_S[2], inputs_A[2]);
   and_1[11] = new_gate(triple_and, inputs_A[2], inputs_S[3], inputs_B[2]);
   and_1[12] = new_gate(and, inputs_B[3], inputs_S[0]);
   and_1[13] = new_gate(and, inputs_S[1], not_0[4]);
   and_1[14] = new_gate(triple_and, not_0[4], inputs_S[2], inputs_A[3]);
   and_1[15] = new_gate(triple_and, inputs_A[3], inputs_S[3], inputs_B[3]);

   // third row: nor gates
   GATE* nor_2[8];

   nor_2[0] = new_gate(triple_nor, inputs_A[0], and_1[0], and_1[1]);
   nor_2[1] = new_gate(nor, and_1[2], and_1[3]);
   nor_2[2] = new_gate(triple_nor, inputs_A[1], and_1[4], and_1[5]);
   nor_2[3] = new_gate(nor, and_1[6], and_1[7]);
   nor_2[4] = new_gate(triple_nor, inputs_A[2], and_1[8], and_1[9]);
   nor_2[5] = new_gate(nor, and_1[10], and_1[11]);
   nor_2[6] = new_gate(triple_nor, inputs_A[3], and_1[12], and_1[13]);
   nor_2[7] = new_gate(nor, and_1[14], and_1[15]);

   // fourth row:
   GATE* row_3[19];

   row_3[0] = new_gate(nand, input_C, not_0[0]);
   row_3[1] = new_gate(xor, nor_2[0], nor_2[1]);
   row_3[2] = new_gate(and, not_0[0], nor_2[0]);
   row_3[3] = new_gate(triple_and, not_0[0], nor_2[1], input_C);
   row_3[4] = new_gate(xor, nor_2[2], nor_2[3]);
   row_3[5] = new_gate(and, not_0[0], nor_2[2]);
   row_3[6] = new_gate(triple_and, not_0[0], nor_2[0], nor_2[3]);
   row_3[7] = new_gate(quad_and, not_0[0], input_C, nor_2[1], nor_2[3]);
   row_3[8] = new_gate(xor, nor_2[4], nor_2[5]);
   row_3[9] = new_gate(and, not_0[0], nor_2[4]);
   row_3[10] = new_gate(triple_and, not_0[0], nor_2[2], nor_2[5]);
   row_3[11] = new_gate(quad_and, not_0[0], nor_2[0], nor_2[3], nor_2[5]);
   row_3[12] = new_gate(quint_and, not_0[0], input_C, nor_2[1], nor_2[3], nor_2[5]);
   row_3[13] = new_gate(xor, nor_2[6], nor_2[7]);
   row_3[14] = new_gate(quad_nand, nor_2[1], nor_2[3], nor_2[5], nor_2[7]);
   row_3[15] = new_gate(quint_nand, input_C, nor_2[1], nor_2[3], nor_2[5], nor_2[7]);
   row_3[16] = new_gate(quad_and, nor_2[0], nor_2[2], nor_2[3], nor_2[7]);
   row_3[17] = new_gate(triple_and, nor_2[2], nor_2[5], nor_2[7]);
   row_3[18] = new_gate(and, nor_2[4], nor_2[7]);

   // fifth row:
   GATE* nor_4[4];

   nor_4[0] = new_gate(nor, row_3[2], row_3[3]);
   nor_4[1] = new_gate(triple_nor, row_3[5], row_3[6], row_3[7]);
   nor_4[2] = new_gate(quad_nor, row_3[9], row_3[10], row_3[11], row_3[12]);
   nor_4[3] = new_gate(quad_nor, row_3[16], row_3[17], row_3[18], nor_2[6]);

   // sizth row:
   GATE* row_5[5];

   row_5[0] = new_gate(xor, row_3[0], row_3[1]);
   row_5[1] = new_gate(xor, nor_4[0], row_3[4]);
   row_5[2] = new_gate(xor, nor_4[1], row_3[8]);
   row_5[3] = new_gate(xor, nor_4[2], row_3[13]);
   row_5[4] = new_gate(nand, row_3[14], nor_4[3]);  // unsure about this one

   // seventh row:
   GATE* and_6 = new_gate(quad_and, row_5[0], row_5[1], row_5[2], row_5[3]);

   // this is a link only, don't free them here
   GATE* outputs_F[4];
   outputs_F[0] = row_5[0];
   outputs_F[1] = row_5[1];
   outputs_F[2] = row_5[2];
   outputs_F[3] = row_5[3];

   GATE* output_P = row_3[14];
   GATE* output_C = row_5[4];
   GATE* output_G = nor_4[3];
   GATE* output_A_B = and_6;

   for (int i = 0; i < 4; i++)
      compute_output(outputs_F[i]);

   compute_output(output_P);
   compute_output(output_C);
   compute_output(output_G);
   compute_output(output_A_B);

   printf("Output F = %u, %u, %u, %u\n", outputs_F[0]->output,
          outputs_F[1]->output, outputs_F[2]->output, outputs_F[3]->output);
   printf("Output P, C, G, 'A = B' = %u, %u, %u, %u\n", output_P->output,
          output_C->output, output_G->output, output_A_B->output);

   // free all gates
   for (int i = 0; i < 4; i++) {
      free_gate(inputs_S[i]);
      free_gate(inputs_A[i]);
      free_gate(inputs_B[i]);
   }
   free_gate(input_M);
   free_gate(input_C);
   for (int i = 0; i < 5; i++) free_gate(not_0[i]);
   for (int i = 0; i < 16; i++) free_gate(and_1[i]);
   for (int i = 0; i < 8; i++) free_gate(nor_2[i]);
   for (int i = 0; i < 19; i++) free_gate(row_3[i]);
   for (int i = 0; i < 4; i++) free_gate(nor_4[i]);
   for (int i = 0; i < 5; i++) free_gate(row_5[i]);
   free_gate(and_6);
}

VERSION = 0.1
INCS = -I ../include
LIBS = -lpierre -lmini_computer
CFLAGS += -std=c17 ${INCS} -DVERSION=\"${VERSION}\" -Ofast -Wall -Wextra -Wno-unused-parameter -Wno-unused-variable
LDFLAGS += ${LIBS}
DEBUG_CFLAGS = ${CFLAGS} -O0 -g -ggdb -Wall -Wextra -Wno-unused-parameter
CC = gcc
